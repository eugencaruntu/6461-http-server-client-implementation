import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static java.lang.Integer.parseInt;

public class Response {
    private StringBuilder response;
    Charset utf8 = StandardCharsets.UTF_8;

    public  Response(SocketChannel socket){
        response = new StringBuilder();
        try {
            ByteBuffer buf = ByteBuffer.allocate(16);
            buf.clear();
            int bytesRead = socket.read(buf);
            while (bytesRead != -1) {
                buf.flip();
                response.append(utf8.decode(buf).toString());
                buf.clear();
                bytesRead = socket.read(buf);
            }
            socket.shutdownInput();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getResponse() {
        return response.toString();
    }

    /**
     * Determine the redirect code and location from response
     * and re-assemble the command
     *
     * @param response
     */
    public boolean detectRedirect(String  response) {
        int code = responseCode(response);
        if (code == 302 || code == 301) {
            try {
                parseURL(new URL(find(response, "Location: ", "\r\n")));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds response code
     *
     * @param response
     * @return the integer corresponding to response code
     */
    private static int responseCode(String response) {
        return parseInt(find(response, "HTTP/1.1 ", " "));
    }

}

