import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class Connection {
    private String host;
    private int port;
    private SocketChannel socket;

    public SocketChannel getSocket() {
        return socket;
    }

    public Connection(String host, int port) throws IOException {
        this.host = host;
        this.port = port;
        socket = SocketChannel.open();
        socket.connect(new InetSocketAddress(host, port));
    }

}
