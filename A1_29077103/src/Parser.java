import joptsimple.OptionParser;
import joptsimple.OptionSet;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import static java.util.Arrays.asList;

public class Parser {
    private static String host;
    private static int port;
    private static String mode;
    private static String url;
    private static String protocol;
    private static String header;
    private static String data;

    private static boolean verbose = false;
    private static boolean fileOutput = false;
    private static File file;
    private static String output;

    private static final String eol = "\r\n";


    public Parser(String[] args) throws IOException {
        /* option parser */
        OptionParser parser = new OptionParser();
        parser.allowsUnrecognizedOptions();

        /* recognized options */
        parser.acceptsAll(asList("?", "help"), "prints this screen.").withOptionalArg().defaultsTo("").forHelp();
        parser.acceptsAll(asList("v", "verbose"), "Prints the detail of the getResponse such as protocol, status, and headers.").withOptionalArg().defaultsTo("true");
        parser.acceptsAll(asList("h", "header"), "Associates headers to HTTP Request with the format 'key:value'.").withRequiredArg();
        parser.acceptsAll(asList("d", "data"), "Associates an inline data to the body HTTP POST sendRequest.").withRequiredArg();
        parser.acceptsAll(asList("f", "file"), "Associates the content of a file to the body HTTP POST sendRequest.").withRequiredArg();
        parser.acceptsAll(asList("o", "output"), "Writes the getResponse to file.").withRequiredArg();
        parser.acceptsAll(asList("p", "protocol"), "Protocol protocol").withOptionalArg().defaultsTo("HTTP/1.0");/* parse the arguments */

        OptionSet opts = parser.parse(args);

        mode = args[0].toUpperCase();
        protocol = (String) opts.valueOf("p");
        parseURL(new URL(findURL(args)));

        /* add additional header entries */
        if (opts.has("h") && opts.hasArgument("h")) {
            addHeader(String.join(eol, (List<String>) opts.valuesOf("h")));
        }

        /* sending in-line data as string */
        if (opts.has("d") && opts.hasArgument("d")) {
            data = ((String) opts.valueOf("d"));
            addHeader("Content-Length:" + data.length());
        }

        /* read the file and feed it into data */
        if (opts.has("f") && opts.hasArgument("f")) {
            file = new File((String) opts.valueOf("f"));
            addHeader("Content-Length:" + file.length());
            addHeader("Content-Disposition:attachment");
        }

        /* set verbosity */
        if (opts.has("v")) {
            verbose = true;
        }

        /* set output */
        if (opts.has("o") && opts.hasArgument("o")) {
            fileOutput = true;
            output = (String) opts.valueOf("o");
        }
    }

    /**
     * Find url within an array, usually args[]
     *
     * @param array
     * @return the url if found
     */
    private static String findURL(String[] array) {
        for (String i : array) {
            int found = i.indexOf("://");
            if (found != -1) {
                return trimSingleQuote(i);
            }
        }
        return "";  // no url found
    }

    /**
     * Parse an URL to determine the path, host, port
     * and set the host in header
     *
     * @param myURL
     */
    private static void parseURL(URL myURL) {
        url = myURL.toString();
        host = myURL.getHost();
        port = (myURL.getPort() == -1) ? myURL.getDefaultPort() : myURL.getPort();
        header = "Host:" + host;
    }

    /**
     * Remove single quote(s) from beginning and end of a string
     *
     * @param s
     * @return
     */
    private static String trimSingleQuote(String s) {
        String tmp = s.replaceAll("^'+", "");          // remove preceding
        tmp = tmp.replaceAll("'+$", "");        // remove trailing
        return tmp;
    }

    /**
     * Add string to header
     *
     * @param toAdd
     */
    private static void addHeader(String toAdd) {
        header = header + eol + toAdd;
    }


    public static String getHost() {
        return host;
    }

    public static int getPort() {
        return port;
    }

    public static String getMode() {
        return mode;
    }

    public static String getUrl() {
        return url;
    }

    public static String getProtocol() {
        return protocol;
    }

    public static String getHeader() {
        return header;
    }

    public static String getData() {
        return data;
    }

    public static boolean isVerbose() {
        return verbose;
    }

    public static boolean isFileOutput() {
        return fileOutput;
    }

    public static File getFile() {
        return file;
    }

    public static String getOutput() {
        return output;
    }

    Connection c = new Connection(host, port);
    Request r = new Request(mode, url, protocol, header, data);
}
