import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

public class Util {
    /**
     * Find url within an array, usually args[]
     *
     * @param array
     * @return the url if found
     */
    private static String findURL(String[] array) {
        for (String i : array) {
            int found = i.indexOf("://");
            if (found != -1) {
                return trimSingleQuote(i);
            }
        }
        return "";  // no url found
    }

    /**
     * Parse an URL to determine the path, host, port
     * and set the host in header
     *
     * @param myURL
     */
    private static void parseURL(URL myURL) {
        url = myURL.toString();
        host = myURL.getHost();
        port = (myURL.getPort() == -1) ? myURL.getDefaultPort() : myURL.getPort();
        header = "Host:" + host;
    }

    /**
     * Remove single quote(s) from beginning and end of a string
     *
     * @param s
     * @return
     */
    private static String trimSingleQuote(String s) {
        String tmp = s.replaceAll("^'+", "");          // remove preceding
        tmp = tmp.replaceAll("'+$", "");        // remove trailing
        return tmp;
    }
    /**
     * Makes a file and writes a string
     *
     * @param output
     * @param content
     */
    private static void writeFile(String output, String content) {
        try {
            PrintWriter writer = new PrintWriter(output, "UTF-8");
            writer.append(content);
            writer.close();
        } catch (
                IOException e) {
            System.out.println(e.getLocalizedMessage());
        }

    }

}