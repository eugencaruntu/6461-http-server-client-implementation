import java.io.File;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;

public class Request {
    private static String cmd;
    private static final char sp = ' ';

    public static String getCmd() {
        return cmd;
    }

    private static final String eol = "\r\n";

    public Request(String mode, String url, String protocol, String header, String data){
        /* put together the request */
        cmd = mode + sp + url + sp + protocol + eol
                + header + eol
                + eol;

        /* if data exits add it to the request body */
        if (data != null) {
            cmd = cmd + data;   // add data inside the string command as body
        }
    }



}
