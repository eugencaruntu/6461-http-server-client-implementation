/**
 * @author: Eugen Caruntu
 * @student_id: 29077103
 * @course: COMP6461
 * @assignment: 1
 */

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketAddress;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.asList;

/**
 * HTTP client implementing GET, POST, HEAD, help
 * <ul>
 * <li> Put together the request
 * <li> Make and endpoint and connect a socket
 * <li> Send the request
 * <li> Receive the response
 * <li> Determine if there is a redirect in response
 * <li> Display the response to screen or write it to file
 * </ul>
 */
public class httpc {
    private static boolean verbose = false;
    private static boolean redirect = false;
    private static boolean fileOutput = false;
    private static SocketAddress endpoint;
    private static SocketChannel socket;
    private static String protocol;
    private static int port;
    private static String url;
    private static String host;
    private static String mode;
    private static String data;
    private static File file;
    private static String output;
    private static String header;
    private static String cmd;
    private static final char sp = ' ';
    private static final String eol = "\r\n";

    public static void main(String[] args) throws IOException {

        /* TESTS */
        //Content-Type: text/plain
        //Content-Type: application/zip
        //Content-Type: multipart/form-data
        //Content-Disposition: form-data
        // httpc (client.get|client.get.post) [-v] (-h "k:v")* [-d inline-data] [-f file] url
        // java -jar comp6461.jar httpc get http://httpbin.org/status/418
        // java -jar comp6461.jar httpc post -h Content-Type:application/json --d '{"Assignment": 1}' http://httpbin.org/post

        //curl -X POST -v -H Content-Type:multipart/form-data;boundary=SOME_BOUNDARY -d @c:\Users\eugen\Documents\git\comp6461\A1_29077103\src\logo.png http://httpbin.org/post

        //String[] test_args = {"get", "-v", "http://httpbin.org/status/418"};
        //String[] test_args = {"get", "-v", "http://httpbin.org/redirect-to?url=http://cnn.com"};
        String[] test_args = {"get", "-v", "http://httpbin.org/redirect-to?url=http://cnn.com", "-o", "c:\\Users\\eugen\\Documents\\git\\comp6461\\A1_29077103\\src\\redirection.txt"};
        //String[] test_args = {"-?"};
        //String[] test_args = {"help", "post"};
        //String[] test_args = {"HEAD", "'http://httpbin.org/get?course=networking&assignment=1'"};
        //String[] test_args = {"GET", "-v", "'http://httpbin.org/get?course=networking&assignment=1'"};
        //String[] test_args = {"POST", "-h", "Content-Type:application/json", "--d", "{\"Assignment\": 1}", "http://httpbin.org/post"};
        //String[] test_args = {"POST", "-v", "-h", "Content-Type:application/json", "--d", "{\"Assignment\": 1}", "http://httpbin.org/post"};
        //String[] test_args = {"POST", "-v", "-h", "Content-Type:text/plain", "-h", "Accept: application/json", "-f", "c:\\Users\\eugen\\Documents\\git\\comp6461\\A1_29077103\\src\\TextFile.txt", "http://httpbin.org/post"};
        //String[] test_args = {"POST", "-v", "-h", "Content-Type:image/png;boundary=xxBOUNDARYxx", "-f", "c:\\Users\\eugen\\Documents\\git\\comp6461\\A1_29077103\\src\\logo.png", "http://httpbin.org/post"};
        //String[] test_args = {"POST", "-v", "-h", "Content-Type:image/png;boundary=xxBOUNDARYxx", "-f", "c:\\Users\\eugen\\Documents\\git\\comp6461\\A1_29077103\\src\\logo.png", "-o", "c:\\Users\\eugen\\Documents\\git\\comp6461\\A1_29077103\\src\\logoresonse.txt", "http://httpbin.org/post"};
        args = test_args;

        /**/
        try {
            parse(args);
            do {
                assembleRequest();
                connectSocket(endpoint);
                sendRequest();
                String response = getResponse();
                process(response);  // we might need to process the response only when redirects are exhausted
                detectRedirect(response);
            } while (redirect);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getClass());
        }
    }

    /**
     * Parsing arguments
     *
     * @param args
     */
    private static void parse(String[] args) throws Exception {
        /* option parser */
        OptionParser parser = new OptionParser();
        parser.allowsUnrecognizedOptions();

        /* recognized options */
        parser.acceptsAll(asList("?", "help"), "prints this screen.").withOptionalArg().defaultsTo("").forHelp();
        parser.acceptsAll(asList("v", "verbose"), "Prints the detail of the getResponse such as protocol, status, and headers.").withOptionalArg().defaultsTo("true");
        parser.acceptsAll(asList("h", "header"), "Associates headers to HTTP Request with the format 'key:value'.").withRequiredArg();
        parser.acceptsAll(asList("d", "data"), "Associates an inline data to the body HTTP POST sendRequest.").withRequiredArg();
        parser.acceptsAll(asList("f", "file"), "Associates the content of a file to the body HTTP POST sendRequest.").withRequiredArg();
        parser.acceptsAll(asList("o", "output"), "Writes the getResponse to file.").withRequiredArg();
        parser.acceptsAll(asList("p", "protocol"), "Protocol protocol").withOptionalArg().defaultsTo("HTTP/1.0");/* parse the arguments */

        OptionSet opts = parser.parse(args);

        /* detect help request or errors and stop execution */
        detectHelp(args);

        /* detect errors and auto-generate entire help with jOpt-Simple */
        if (opts.has("?") || (opts.has("d") && opts.has("f"))) {
            try {
                System.out.println("Either [-d] or [-f] can be used but not both.\n");
                parser.printHelpOn(System.out);
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        mode = args[0].toUpperCase();
        protocol = (String) opts.valueOf("p");
        parseURL(new URL(findURL(args)));

        /* add additional header entries */
        if (opts.has("h") && opts.hasArgument("h")) {
            addHeader(String.join(eol, (List<String>) opts.valuesOf("h")));
        }

        /* sending in-line data as string */
        if (opts.has("d") && opts.hasArgument("d")) {
            data = ((String) opts.valueOf("d"));
            addHeader("Content-Length:" + data.length());
        }

        /* read the file and feed it into data */
        if (opts.has("f") && opts.hasArgument("f")) {
            file = new File((String) opts.valueOf("f"));
            addHeader("Content-Length:" + file.length());
            addHeader("Content-Disposition:attachment");
        }

        /* set verbosity */
        if (opts.has("v")) {
            verbose = true;
        }

        /* set output */
        if (opts.has("o") && opts.hasArgument("o")) {
            fileOutput = true;
            output = (String) opts.valueOf("o");
        }
    }

    /**
     * Find url within an array, usually args[]
     *
     * @param array
     * @return the url if found
     */
    private static String findURL(String[] array) {
        for (String i : array) {
            int found = i.indexOf("://");
            if (found != -1) {
                return trimSingleQuote(i);
            }
        }
        return "";  // no url found
    }

    /**
     * Parse an URL to determine the path, host, port
     * and set the host in header
     *
     * @param myURL
     */
    private static void parseURL(URL myURL) {
        url = myURL.toString();
        host = myURL.getHost();
        port = (myURL.getPort() == -1) ? myURL.getDefaultPort() : myURL.getPort();
        header = "Host:" + host;
    }

    /**
     * Remove single quote(s) from beginning and end of a string
     *
     * @param s
     * @return
     */
    private static String trimSingleQuote(String s) {
        String tmp = s.replaceAll("^'+", "");          // remove preceding
        tmp = tmp.replaceAll("'+$", "");        // remove trailing
        return tmp;
    }

    /**
     * assemble the request
     */
    private static void assembleRequest() {
        /* make the endpoint */
        endpoint = new InetSocketAddress(host, port);

        /* put together the request */
        cmd = mode + sp + url + sp + protocol + eol
                + header + eol
                + eol;

        /* if data exits add it to the request body */
        if (data != null) {
            cmd = cmd + data;   // add data inside the string command as body
        }
        // System.out.println(cmd);
    }

    /**
     * Opens a SocketChannel and connects the endpoint
     *
     * @param endpoint
     * @throws IOException
     */
    private static void connectSocket(SocketAddress endpoint) throws IOException {
        socket = SocketChannel.open();
        socket.connect(endpoint);
    }

    /**
     * writes the requet into a ByteBuffer and puts it through the socket
     *
     * @param
     * @throws IOException
     */
    private static void sendRequest() throws IOException {
        try {
            /* put the command and inline data into the buffer */
            ByteBuffer buf = ByteBuffer.wrap(cmd.getBytes());
            socket.write(buf);
            buf.clear();

            /* if file, stream the file into the buffer */
            if (file != null) {
                FileInputStream fis = new FileInputStream(file);
                FileChannel fileChannel = fis.getChannel();
                ByteBuffer buffer = ByteBuffer.allocate(64);
                int bytesRead = fileChannel.read(buffer);
                while (bytesRead != -1) {
                    buffer.flip();
                    while (buffer.hasRemaining()) {
                        socket.write(buffer);
                    }
                    buffer.clear();
                    bytesRead = fileChannel.read(buffer);
                }
                fis.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * reads the buffer and decodes the bytes as they are coming through the socket
     *
     * @throws IOException
     */
    private static String getResponse() throws IOException {
        Charset utf8 = StandardCharsets.UTF_8;
        StringBuilder response = new StringBuilder();
        try {
            ByteBuffer buf = ByteBuffer.allocate(16);
            buf.clear();
            int bytesRead = socket.read(buf);
            while (bytesRead != -1) {
                buf.flip();
                response.append(utf8.decode(buf).toString());
                buf.clear();
                bytesRead = socket.read(buf);
            }
            socket.shutdownInput();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    /**
     * Add string to header
     *
     * @param toAdd
     */
    private static void addHeader(String toAdd) {
        header = header + eol + toAdd;
    }

    /**
     * Takes the getResponse string and slipts it at \r\n\r\n
     *
     * @param response
     * @return the array of header and body
     */
    private static String[] splitResponse(String response) {
        String[] split = response.split("\r\n\r\n");
        return split;
    }

    /**
     * Makes a file and writes a string
     *
     * @param output
     * @param content
     */
    private static void writeFile(String output, String content) {
        try {
            PrintWriter writer = new PrintWriter(output, "UTF-8");
            writer.append(content);
            writer.close();
        } catch (
                IOException e) {
            System.out.println(e.getLocalizedMessage());
        }

    }

    /**
     * Determine the redirect code and location from response
     * and re-assemble the command
     *
     * @param response
     */
    private static void detectRedirect(String response) {
        int code = responseCode(response);
        if (code == 302 || code == 301) {
            redirect = true;
            System.out.println("found: " + responseCode(response));
            try {
                parseURL(new URL(find(response, "Location: ", "\r\n")));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            redirect = false;
        }
    }

    /**
     * Finds a substring between 'from' and 'to' arguments
     *
     * @param where
     * @param from
     * @param to
     * @return
     */
    private static String find(String where, String from, String to) {
        int start = where.indexOf(from) + from.length();
        String remainder = where.substring(start);
        int end = remainder.indexOf(to);
        return remainder.substring(0, end);
    }

    /**
     * Finds response code
     *
     * @param response
     * @return the integer corresponding to response code
     */
    private static int responseCode(String response) {
        return parseInt(find(response, "HTTP/1.1 ", " "));
    }

    /**
     * Writes the getResponse on file or screen
     * depending on CLI for output and verbosity
     *
     * @param response
     */
    private static void process(String response) {
        if (fileOutput) {
            writeFile(output, response);
            System.out.println("The getResponse was written to " + output);
        } else {
            if (verbose || mode.toLowerCase() == "head") {  // if verbose print entire getResponse
                System.out.println(response);
            } else {   // if not verbose split getResponse and print only the body
                System.out.println(splitResponse(response)[1]);
            }
        }
    }

    /**
     * detect the help request and show corresponding help version
     *
     * @param args
     */
    private static void detectHelp(String[] args) {
        if (args[0].toLowerCase() == "help" && args.length > 1) {
            if (args[1].toLowerCase() == "get")
                printGetHelp();
            if (args[1].toLowerCase() == "post")
                printPostHelp();
            else
                printHelp();
            System.exit(0);
        } else if (args[0].toLowerCase() == "help") {
            printHelp();
            System.exit(0);
        }
    }

    /**
     * Print generic help
     */
    private static void printHelp() {
        StringBuilder help = new StringBuilder();
        help.append("httpc is a curl-like application but supports HTTP protocol only.\n");
        help.append("Usage:\thttpc command [arguments] The commands are:\n");
        help.append("\t\tget\t\texecutes a HTTP GET request and prints the response.\n");
        help.append("\t\tpost\texecutes a HTTP POST request and prints the response.\n");
        help.append("\t\thelp\tprints this screen.\n");
        help.append("Use \"httpc help [command]\" for more information about a command.\n");
        System.out.print(help.toString());
    }

    /**
     * Print GET Usage
     */
    private static void printGetHelp() {
        StringBuilder help = new StringBuilder();
        help.append("usage: httpc get [-v] [-h key:value] URL\n");
        help.append("Get executes a HTTP GET request for a given URL.\n");
        help.append("\t-v\t\t\t\tPrints the detail of the response such as protocol, status, and headers.\n");
        help.append("\t-h key:value\tAssociates headers to HTTP Request with the format 'key:value'.\n");
        System.out.print(help.toString());
    }

    /**
     * Print POST Usage
     */
    private static void printPostHelp() {
        StringBuilder help = new StringBuilder();
        help.append("usage: httpc post [-v] [-h key:value] [-d inline-data] [-f file] URL\n");
        help.append("Post executes a HTTP POST request for a given URL with inline data or from file.\n");
        help.append("\t-v\t\t\t\tPrints the detail of the response such as protocol, status, and headers.\n");
        help.append("\t-h key:value\tAssociates headers to HTTP Request with the format 'key:value'.\n");
        help.append("\t-d\t\t\t\tstring Associates an inline data to the body HTTP POST request.\n");
        help.append("\t-f\t\t\t\tfile Associates the content of a file to the body HTTP POST request.\n");
        help.append("Either [-d] or [-f] can be used but not both.\n");
        System.out.print(help.toString());
    }
}